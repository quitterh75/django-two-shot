from django.urls import path
from receipts.views import (
    receipt_listview,
    create_receipt_listview,
    category_list,
    accounts_list,
    create_category,
    create_account,
)

urlpatterns = [
    path("receipts/", receipt_listview, name="home"),
    path("receipts/create/", create_receipt_listview, name="create_receipt"),
    path("receipts/categories/", category_list, name="category_list"),
    path("receipts/accounts/", accounts_list, name="account_list"),
    path(
        "receipts/categories/create/", create_category, name="create_category"
    ),
    path("receipts/accounts/create/", create_account, name="create_account"),
]
