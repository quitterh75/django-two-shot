from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory


# Register your models here.
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
    )


@admin.register(ExpenseCategory)
class ExpeneseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)
